
require('app-module-path').addPath(__dirname + '/app');
require('app-module-path').addPath(__dirname + '/modules');

var http = require('http');
var config = require('config');
var log = require('logger')(module);

/**
 * Listen on provided port, on all network interfaces.
 */
(function(){
    var app = require('server').configure(config);

    var server = http.createServer(app);
    server.listen(app.get('port'));
    server.on('error', function (error) {
        //var addr = server.address();
        //if (error.syscall !== 'listen') {
        //    throw error;
        //}
        //
        //var bind = typeof addr === 'string'
        //    ? 'pipe ' + addr
        //    : 'port ' + addr.port;

        // handle specific listen errors with friendly messages
        switch (error.code) {
            case 'EACCES':
                console.error('requires elevated privileges');
                process.exit(1);
                break;
            case 'EADDRINUSE':
                console.error('is already in use');
                process.exit(1);
                break;
            default:
                throw error;
        }
    });
    server.on('listening', function () {
        var addr = server.address();
        var bind = typeof addr === 'string'
            ? 'pipe ' + addr
            : 'port ' + addr.port;
        log.info('Listening on ' + bind);
    });
})();



