(function () {
    'use strict';
    /**
     * Module to start up application
     */
    angular
        .module('app.common', [
            'ui.router'
        ])


})();