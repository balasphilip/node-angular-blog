(function(){
    'use strict';

    angular
        .module('app.control')
        .controller('AuthCtrl', AuthCtrl);

    AuthCtrl.$inject = ['$scope', '$window', 'Authentication', 'Registration'];

    /* @ngInject */
    function AuthCtrl($scope, $window, Authentication, Registration) {

        $scope.user = {};
        $scope.logIn = logIn;
        $scope.signIn = signIn;


        function logIn(Form) {
            if (Form.$valid) {
                $scope.error = false;
                Authentication.login($scope.user)
                    .success(function (res) {
                        if (res.user && res.user.token) {
                            Authentication.setToken(res.user.token);
                            $window.location.href = '/admin/';
                        }
                    })
                    .error(function (err) {
                        console.log(err);
                        $scope.error = err.message;
                    });
            }
        }

        function signIn(Form) {
            if (Form.$valid) {
                $scope.error = false;
                Registration.save($scope.user)
                    .success(function (res) {
                        if (res) {
                            $scope.isSignIn = false;
                        }
                    })
                    .error(function (err) {
                        console.log(err);
                        $scope.error = err.message;
                    });
            }
        }
    }
})();
