(function(){
    'use strict';

    angular
        .module('app.control')
        .controller('PostsCtrl', PostsCtrl);

    PostsCtrl.$inject = ['$scope', '$state', 'Authentication', 'Registration'];

    /* @ngInject */
    function PostsCtrl($scope, $http) {
        $http.get('/admin/post');

    }
})();
