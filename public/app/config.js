angular
    .module('app.common',[])
    .constant('appConfig', {
        uiRoutes: {
            control: {
                parent: 'layout',
                children: {
                    'posts': 'posts',
                    'posts/:name': 'posts',
                    'editor': 'editor',
                    'settings': 'settings'
                }
            },
            login: {
                login: 'auth',
                'reset-password': 'auth'
            },
            blog: {}
        },
        params: appConfig || {}
    });