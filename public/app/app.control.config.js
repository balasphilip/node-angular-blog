(function () {'use strict';
    angular
        .module('app.control')
        .config(configBlock);

    configBlock.$inject = [
        '$httpProvider',
        '$locationProvider',
        'localStorageServiceProvider',
        'appLauncherProvider'

    ];
    function configBlock ($httpProvider,
                          $locationProvider,
                          localStorageServiceProvider,
                          appLauncherProvider
      ) {

        $locationProvider.html5Mode(true);
        $httpProvider.interceptors.push('authInterceptor');
        $httpProvider.interceptors.push('responseInterceptor');
        localStorageServiceProvider.setPrefix('spb'); // = simple Philip's blog
        appLauncherProvider.initialize();

    }
})();
