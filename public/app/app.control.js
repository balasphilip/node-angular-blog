(function () {
    'use strict';
    /**
     * Module to start up application
     */
    angular
        .module('app.control', [
            'LocalStorageModule',
            'angularFileUpload',
            'ui.router',
            'app.common'
        ])
        .factory('authInterceptor', authInterceptor)
        .factory('responseInterceptor', responseInterceptor);

    authInterceptor.$inject = ['$window', '$q', 'localStorageService'];
    function authInterceptor ($window, $q, localStorageService) {

        return {
            // Add authorization token to headers
            request: function (config) {
                config.headers = config.headers || {};
                if (localStorageService.get('token')) {
                    config.headers['x-access-token'] = localStorageService.get('token');
                }
                return config;
            },
            // Intercept 401s and redirect you to login or to landing page
            responseError : function (response) {
                if (response !== null && response.status === 401) {
                    localStorageService.remove('token');
                    $window.location.href = '/admin/login';
                }
                return $q.reject(response);
            }
        };
    }

    responseInterceptor.$inject = ['$rootScope', '$q', 'localStorageService', '$window'];
    function responseInterceptor ($rootScope, $q, localStorageService, $window) {
        return {
            response : function (response) {
                if (response.config.url === 'check-auth' && (!response.data || !response.data.success)) {
                    return $q.reject({
                        status: 401,
                        message: 'User is not authorized'
                    });
                }
                return response;
            }
        };
    }

})();