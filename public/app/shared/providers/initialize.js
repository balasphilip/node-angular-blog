(function () {
    'use strict';

    angular
        .module('app.common')
        .provider('appLauncher', appLauncherProvider);

    appLauncherProvider.$inject = ['$stateProvider', '$urlRouterProvider', 'appConfig'];

    /* @ngInject */
    function appLauncherProvider($stateProvider, $urlRouterProvider, appConfig) {
        var routesBuilder = {
            control: buildControlRoutes,
            login: buildLoginRoutes,
            blog: buildBlogRoutes
        };

        /**
         * Method generates routes for different root urls.
         * Generating routes for main page ('/') based on config (Config.js),
         * property: uiRoutes, where key is future route and value is item on left-hand
         * menu panel. If you don't want to generate item, set value as null. Also, you
         * should to remember, that each item in menu is connected with user's role. So if user
         * is not admin, then we should to connect the item
         * and the role - app/auth/schemes/control-panel/_restrictions-config.json.
         * Generating is performing with next rules:
         * route - as is.
         * controller - camelCase with first capitalized letter.
         * path to template - /admin-app/views/ + directory + camelCase with first small letter + tpl. prefix.
         *
         * Route 'application-details' transforms to:
         * route - '/application-details'
         * controller - 'ApplicationDetailsController'
         * path to template - /admin-app/views/application/applicationDetails.tpl.html
         *
         */
        this.initialize = function() {
            var main;

            if (!appConfig.params || !appConfig.params.page) {
                throw new Error('Can\'t build routes. Type of application is not provided!')
            }

            main = appConfig.params.page;
            routesBuilder[main](appConfig.uiRoutes[main]);
        };

        this.$get = function () {
            return {
                menuItems: {}
            };
        };

        function buildControlRoutes() {
            $stateProvider
                .state('layout', {
                    abstract: true,
                    url: '/',
                    template:'<div ui-view ></div>',
                    resolve:{
                        checkAuth:  function($http){
                            return $http.get('check-auth');
                        }
                    },
                    controller: ['$scope', 'checkAuth', '$rootScope',
                        function($scope, checkAuth, $rootScope) {
                            console.log(checkAuth)
                            if (checkAuth.data && checkAuth.data.success) {
                                $rootScope.isLogged = true
                            }
                        }
                    ]
                })
                .state('layout.posts', {
                    url: 'posts',
                    controller: function($scope, checkAuth, $stateParams) {
                        console.log('posts');
                    }})
                .state('layout.editor', {
                    url: 'editor',
                    controller: function($scope, checkAuth) {
                        console.log('editor')
                    }})
                .state('layout.settings', {
                    url: 'settings',
                    controller: function($scope, checkAuth) {
                        console.log('settings')
                    }})
            $urlRouterProvider.otherwise('/posts');
        }

        function buildLoginRoutes(config) {
            $stateProvider
                .state('login', {
                    url: '/login',
                    templateUrl: '/app/components/auth/login.tpl.html',
                    controller: 'AuthCtrl'
                })
                .state('reset', {
                    url: '/reset-password',
                    templateUrl: '/app/components/auth/reset-password.tpl.html',
                    controller: 'AuthCtrl'
                });
            $urlRouterProvider.otherwise('/login');
        }
        function buildBlogRoutes() {

        }

        /**
         * Normalize route name to controller name
         * @param item
         * @param splitter
         * @returns {String|Array}
         */
        function normalizeCtrlName(item, splitter) {
            if (splitter) {
                return item
                    .split('-')
                    .map(function (part) {
                        return part[0].toUpperCase() + part.slice(1);
                    })
                    .join('');
            }
            return item[0].toUpperCase() + item.slice(1);
        }

        /**
         * Normalize route name to template name
         * @param item - route
         * @param splitter
         * @returns {String}
         */
        function normalizeTemplateName(item, splitter) {
            var splitted = item.split('-'),
                notNeedModify = splitted.splice(0,1),
                needModify = splitted
                    .map(function (part) {
                        return part[0].toUpperCase() + part.slice(1);
                    });
            return notNeedModify.concat(needModify).join('');
        }

    }
})();



