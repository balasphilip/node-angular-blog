(function () {
    'use strict';

    angular
        .module('app.control')
        .factory('Authentication', AuthenticationService);

    AuthenticationService.$inject = ['$http', '$window', 'localStorageService'];

    /* @ngInject */
    function AuthenticationService($http, $window, localStorageService) {
        var service = {
            login: login,
            reset: reset,
            logout: logout,
            setToken: setToken,
            getToken: getToken
        };

        return service;


        function login(user) {
            return $http({
                method: 'POST',
                url   : 'login',
                data  : user
            });
        }

        function reset(email) {
            return $http({
                method: 'POST',
                url   : 'reset-password',
                data  : email
            });
        }

        function logout() {
            $http({
                method: 'POST',
                url   : 'logout'
            }).then(function () {
                $window.location.href = '/admin/login';
            }).catch(function (err) {
                console.log(err);
            });
        }

        function setToken(val) {
            localStorageService.set('token', val);
        }

        function getToken(val) {
            localStorageService.get('token');
        }

    }
})();

