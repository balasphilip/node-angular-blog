(function () {
    'use strict';

    angular
        .module('app.control')
        .factory('Registration', RegistrationService);

    RegistrationService.$inject = ['$http', '$window'];

    /* @ngInject */
    function RegistrationService($http, $window) {
        var service = {
            save: save
        };

        return service;


        function save(user) {
            return $http({
                method: 'POST',
                url   : '/admin/save',
                data  : user
            });
        }
    }
})();

