'use strict';

var User = require('models/user');
var log = require('logger')(module);
var messageBus = require('message-bus');

module.exports = {
    loginPage: function (req, res, next) {
        res.render('index', {
            layout: 'login',
            appConfig: {
                page: 'login'
            }
        });
    },
    login: function (req, res, next) {
        User.performAuthCheck(req.body.email,req.body.password)
            .then(function (result) {
                if (!result.success) {
                    res.status(400).json({success: false, message: result.message});
                    return;
                }
                messageBus.emit('user:authenticated');
                res.json({success: true, user: result.user});
            })
            .catch(function (err) {
                log.error('Error occurs during authentication', err);
                res.status(500).json({success: false, error: 'Error occurs during authentication'});
            });

    },
    logout: function (req, res, next) {
         res.redirect('/login');
    },
    resetPassword: function (req, res, next) {
        res.render('index', { title: 'Reset password' });
    },
    checkAuth: function (req, res, next) {
        res.json({success: true});
    }
};