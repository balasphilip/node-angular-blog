'use strict';

var log = require('logger')(module);
var Promise = require('bluebird');


module.exports = {
    errorHandler: function (err, req, res, next) {
        log.warn(req.errorText);
        //next();
        //res.status(400);
        //res.send({error: req.errorText});
        res.status(400).json({ error:'my custom error' });

    }
};