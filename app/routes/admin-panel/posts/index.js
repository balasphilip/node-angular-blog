'use strict';

var Post = require('models/post');
var log = require('logger')(module);


module.exports = {
    index: function (req, res) {
        res.render('index', {
            appConfig: {
                page: 'control'
            }
        });
    },
    getPost: function (req, res) {
        Post.find({})
            .then(function (posts) {
                res.json({posts: posts});
            });
    },
    saveDraft: function (req, res) {
        var post = new Post({
            title: req.body.title,
            author: req.body.author,
            body: req.body.body,
            published: false
        });

        post.saveP()
            .then(function (post) {
                res.json({success: true, post: post});
            })
            .catch(function (err) {
                log.warn('Validation error', {message: err.message});
                res.status(400).json({"error": true, message: {type: err.name, message: err.message}});
            });
    },
    publishPost: function (req, res, next) {
        res.send('post ' + req.body.id + ' has been published');
    }
}