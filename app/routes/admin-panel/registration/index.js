'use strict';

var express = require('express');
var log = require('logger')(module);
var User = require('models/user');

module.exports = {

    save: function (req, res) {
        var admin = new User({
            email: req.body.email,
            password: req.body.password
        });

        admin.saveP()
            .then(function (user) {
                res.json({success: true});
            })
            .catch(function (err) {
                log.warn('Validation error', {message: err.message});
                res.status(400).json({"error": true, message: {type: err.name, message: err.message}});
            });
    },

    remove: function (req, res, next) {
         res.send({success: true});
    }

};
