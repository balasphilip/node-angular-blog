var express = require('express');
var router = express.Router();
var log = require('logger')(module);



module.exports = {
    post: function (req, res, next) {
        log.info(req.params.post)
        res.render('post', { title: req.params.post });
    }
}