'use strict';

var nconf = require('nconf');
var path = require('path');


(function (){
    var env = 'dev';
    nconf.env(env);
    nconf.argv()
        .env()
        .file({ file: path.join(__dirname, env + 'Config.json') });
})();

module.exports = nconf;

