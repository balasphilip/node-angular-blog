'use strict';

var express = require('express');
var path = require('path');
var router = require('router');
var fs = require('fs');
var mongooseConnector = require('mongoose-connector');
var validatorHelpers = require('validator-helpers');
var hbs = require('express-hbs');
var hbsHelpers = require('hbs-helpers');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var messageBus = require('message-bus');
var auth    = require('auth');

function configure(config) {

    var app = express();

    // view engine setup
    app.engine('hbs', hbs.express4({
        partialsDir: process.cwd() + '/app/views/partials',
        layoutsDir: process.cwd() + '/app/views/layouts',
        defaultLayout: process.cwd() + '/app/views/layouts/main',
        extname: '.hbs'
    }));
    app.set('view engine', 'hbs');
    app.set('views', process.cwd() + '/app/views/templates');

    var port = config.get('port') || '3000';
    app.set('port', port);

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));

    app.use(cookieParser());
    app.use(express.static(process.cwd() + '/public/'));

    hbsHelpers.init(hbs);
    mongooseConnector.init();
    validatorHelpers.init();

    app.use('/admin',
        auth.init(config.get('auth')),
        router.bindRoutes(app, 'admin'));

    app.use('/',
        router.bindRoutes(app, 'blog'));

    //app.use(middleware.errors);

    return app;
}


module.exports.configure = configure;