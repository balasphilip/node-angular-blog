'use strict';

var mongoose = require('mongoose-aplus');
var Schema = mongoose.Schema;
var validator = require('validator');
var Promise = require('bluebird');
var log = require('logger')(module);
var config    = require('config');

// create a schema
var postSchema = new Schema({
    title:  { type: String, required: true},
    author: { type: String, required: true},
    body:   { type: String, required: true},
    published: { type: Boolean, required: true},
    comments: [{ body: String, date: Date }],
    date: { type: Date, default: Date.now },
    meta: {
        votes: Number,
        favs:  Number
    }
});

// create static methods
var staticMethods = {

};

// create instance methods
var instanceMethods = {

};

postSchema.statics = staticMethods;
postSchema.methods = instanceMethods;

var Post = mongoose.model('Post', postSchema);

//User.schema.path('email').validate(function (value) {
//    return validator.isEmail(value);
//}, 'Please, enter correct email');

postSchema.pre('save', function(next) {
next();
    //var self = this;
    //
    //User.findOneP({email : this.email}, 'email')
    //    .then(function (res) {
    //        if (!res) {
    //
    //            // get the current date
    //            var currentDate = new Date();
    //            // change the updated_at field to current date
    //            self.updated_at = currentDate;
    //
    //            // if created_at doesn't exist, add to that field
    //            if (!self.created_at) {
    //                self.created_at = currentDate;
    //            }
    //
    //            self.privilege = 'admin';
    //
    //            // hash password
    //            return self.hashPassword()
    //                .then(function (hash) {
    //                    self.password = hash;
    //                    return next();
    //                })
    //                .catch(function (err) {
    //                    log.error('Error when password hashing');
    //                    next(err);
    //                });
    //
    //        }
    //
    //        self.invalidate("email", "email must be unique");
    //        next(new Error("Email must be unique"));
    //
    //    })
    //    .catch(function (err) {
    //        log.error('Error when finding user in db', err);
    //        next(err);
    //    });

});

// make this available to our users in our Node applications
module.exports = Post;