'use strict';

var mongoose = require('mongoose-aplus');
var Schema = mongoose.Schema;
var validator = require('validator');
var Promise = require('bluebird');
var log = require('logger')(module);
var jwt    = require('auth');
var config    = require('config');

var bcrypt = require('bcrypt-as-promised');

// create a schema
var userSchema = new Schema({
    email: { type: String, required: true, unique: true},
    password: { type: String, required: true},
    privilege: String,
    created_at: Date,
    updated_at: Date
});

// create static methods
var staticMethods = {
    performAuthCheck: function (email, pass) {
        var genFn = Promise.coroutine(function* () {
            var user = yield User.findOneP({email: email});

            if (!user) {
                log.error('User is not found', {email: email});
                return {success: false, message: 'Please, check is email correct'};
            }

            try {
                yield user.authenticate(pass);

                var token = User.generateToken(user);
                user = {
                    email: user.email,
                    privilege: user.privilege,
                    created_at: user.created_at,
                    updated_at: user.updated_at,
                    token: token
                };

                log.info('User is authenticated', user);

                return {success: true, user: user};
            } catch(err) {
                log.error('User is not authenticated, password is invalid', {email: email, password: pass});
                return {success: false, message: 'Password is invalid'};
            }

        });

        return genFn();
    },
    generateToken: function (user) {
        var token = jwt.sign(user, config.get('auth:secret'), {
            expiresInMinutes: 1440 // expires in 24 hours
        });
        return token;
    }
};

// create instance methods
var instanceMethods = {
    hashPassword: function () {
        var self = this;
        return bcrypt.genSalt(10)
            .then(function (salt) {
                return bcrypt.hash(self.password, salt)
            });
    },
    authenticate: function (pass) {
        return bcrypt.compare(pass, this.password);
    }
};


userSchema.statics = staticMethods;
userSchema.methods = instanceMethods;

var User = mongoose.model('User', userSchema);

User.schema.path('email').validate(function (value) {
    return validator.isEmail(value);
}, 'Please, enter correct email');

userSchema.pre('save', function(next) {

    var self = this;

    User.findOneP({email : this.email}, 'email')
        .then(function (res) {
            if (!res) {

                // get the current date
                var currentDate = new Date();
                // change the updated_at field to current date
                self.updated_at = currentDate;

                // if created_at doesn't exist, add to that field
                if (!self.created_at) {
                    self.created_at = currentDate;
                }

                self.privilege = 'admin';

                // hash password
                return self.hashPassword()
                    .then(function (hash) {
                        self.password = hash;
                        return next();
                    })
                    .catch(function (err) {
                        log.error('Error when password hashing');
                        next(err);
                    });

            }

            self.invalidate("email", "email must be unique");
            next(new Error("Email must be unique"));

        })
        .catch(function (err) {
            log.error('Error when finding user in db', err);
            next(err);
        });

});

// make this available to our users in our Node applications
module.exports = User;