'use strict';

var jwt = require('jsonwebtoken');
var log = require('logger')(module);
var messageBus = require('message-bus');

var jwtCustom = Object.create(jwt, {
    config : {
        writable: true,
        value: null
    },
    init: {
        value: function(config) {
            var self = this;
            this.config = config;

            messageBus.on('user:authenticated', function () {
                self.config.isLoggedIn = true;
            });

            return function authMiddleware(req,res,next) {

                if (~self.config.skip.indexOf(req.path) || self.config.disable || self.config.isLoggedIn) {
                    log.info('Not check token for: ' + req.path);
                    self.config.isLoggedIn = false;
                    return next();
                }

                var token = req.body.token || req.query.token || req.headers['x-access-token'];

                if (!token && req.method === 'GET') {
                    res.render('index', {
                        appConfig: {
                            page: 'control'
                        }
                    });
                    return;
                }

                if (!token) {
                    // if there is no token
                    // return an error
                    return res.status(401).send({
                        success: false,
                        message: 'No token provided.'
                    });
                }

                // verifies secret and checks exp
                self.verify(token, self.config.secret, function(err, decoded) {

                    if (err) {
                        log.error('Failed to authenticate token.', err);
                        return res.json({ success: false, message: 'Failed to authenticate token.' });
                    }

                    // if everything is good, save to request for use in other routes
                    req.token = decoded;
                    next();

                });
            }
        }
    }
});

module.exports = jwtCustom;