'use strict';

var fs = require('fs');

/**
 * Init all custom helpers for handlebars
 */
module.exports.init = function (hbs) {

    var helpersPath = __dirname + '/helpers';

    // initializing all the helpers
    fs.readdirSync(helpersPath).forEach(function (value) {
        if (fs.lstatSync(helpersPath + '/' + value).isFile()) {
            hbs.registerHelper(value.slice(0, value.indexOf('.')), require(helpersPath + '/' + value));
        }
    });

};
