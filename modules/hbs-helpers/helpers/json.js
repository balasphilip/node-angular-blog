'use strict';

/**
 * Handlebar helper "json". Outputs input object as stringified JSON.
 *
 * Example of usage:
 * {{json input_object}}
 *
 * @param json
 * @returns {*}
 */
module.exports = function (json) {
    return JSON.stringify(json);
};/**
 * Created by mac on 27.07.15.
 */
