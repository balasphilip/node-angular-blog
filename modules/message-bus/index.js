'use strict';

var util         = require("util");
var EventEmitter = require("events").EventEmitter;

function MessageBus () {
    EventEmitter.call(this);
}

util.inherits(MessageBus, EventEmitter);

var events = new MessageBus();

module.exports = events;