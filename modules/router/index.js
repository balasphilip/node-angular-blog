'use strict';

var fs = require('fs');
var express = require('express');
var log = require('logger')(module);

var routerService = {

    pathToRoot: {
        blog: process.cwd() + '/app/routes/blog',
        admin: process.cwd() + '/app/routes/admin-panel'
    },

    bindHandlerToRoute: function(routeName, routes, index, router, middleware) {

        if (routes.middleware && routes.middleware.length) {
            routes.middleware.forEach(function (obj) {
                var type = obj.config.type;
                require('./strategies/' + type + 'Strategy')(router, obj, middleware);
            })
        }

        if (!routes.index) {
            throw new Error('Configuration for router: ' + routeName + ' is not provided');
        }

        routes.index.forEach(function (obj) {
            var type = obj.router.type;
            require('./strategies/' + type + 'Strategy')(router, obj, index);

        });

    },

    bindRoutes: function bindRoutes(app, root) {
        var router = express.Router();
        var rootName = root,
            self =this;

        root = this.pathToRoot[root];

        fs.readdirSync(root).forEach(function (value) {
            var files = fs.readdirSync(root + '/' + value)
            if (files.length >= 2 && ~files.indexOf('index.js') && ~files.indexOf('routes.json')) {
                var index = require(root + '/' + value + '/index');
                var routes = require(root + '/' + value + '/routes');
                if (~files.indexOf('middleware.js')) {
                    var middleware = require(root + '/' + value + '/middleware');
                }
                try {
                    self.bindHandlerToRoute(value, routes, index, router, middleware);
                    log.info('created route: ' + value + ' for ' + rootName + ' panel');
                } catch (err){
                    log.error('Error occurs when binding routes', err)
                }

            }
        });

        return router;
    }
};

module.exports.bindRoutes = routerService.bindRoutes.bind(routerService);