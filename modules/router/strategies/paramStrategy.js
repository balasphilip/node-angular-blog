'use strict';

module.exports = function paramStrategy(router, config, handlers) {

    if (!handlers || !handlers[config.handler]) {
        throw new Error('Handler: ' + config.handler + ' for route with params: ' + config.router.params + ' is not provided');
    }

    router.param(config.config.params, handlers[config.handler]);
};