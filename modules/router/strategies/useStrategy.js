'use strict';

module.exports = function useStrategy(router, config, handlers) {

    if (!handlers || !handlers[config.handler]) {
        throw new Error('Handler: ' + config.handler + ' for route with "use" type is not provided');
    }

    if (config.path) {
        router.use(config.path, handlers[config.handler]);
        return;
    }

    router.use(handlers[config.handler]);
};