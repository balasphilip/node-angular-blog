'use strict';

module.exports = function methodStrategy(router, config, handlers) {

    if (config.router.name.length === 1) {

        if (!handlers || !handlers[config.handler]) {
            throw new Error('Handler: ' + config.handler + ' for route: ' + config.path + ' is not provided');
        }

        router[config.router.name[0].toLowerCase()](config.path, handlers[config.handler]);
        return;
    }

    if (config.router.name.length !== config.handler.length) {
        throw new Error('Count of handlers is not equals number of methods for: ' + config.path);
    }

    var routerInstance = router.route(config.path);
    config.router.name.forEach(function (name, i) {
        routerInstance[name.toLowerCase()](handlers[config.handler[i]]);
    })
};