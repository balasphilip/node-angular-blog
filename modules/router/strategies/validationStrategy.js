'use strict';

var validator = require('validator');
var _ = require('lodash');
var log = require('logger')(module);

// example of validation config:
//"middleware" : [
//    {
//        "path"      : "/save-user",
//        "config"    : {
//            "type"    : "validation",
//            "method"  : ["POST", "PUT"], // or "method" : "POST"
//            "rules"   : {
//                "email"   :[
//                    {
//                        "name": "isEmail",
//                        "text": "email is incorrect"
//                    },
//                    {
//                        "name": "isRequired",
//                        "text": "email is required"
//                    }
//                ],
//                "password": {
//                    "name": "isRequired",
//                    "text": "password is required"
//                }
//            }
//        },
//        "handler": "errorHandler"
//    }
//]

var helpService = {
    validateReqMethod: function (conf, req) {
        if (Array.isArray(conf.config.method)) {
            return ~conf.config.method.indexOf(req.method);
        }
        return conf.config.method === req.method;
    },
    checkData : function (rules, data, req) {
        return Object.keys(rules).every(function (key) {
            if (Array.isArray(rules[key])) {
                for (var i = 0; i < rules[key].length; i+=1) {
                    if (!validator[rules[key][i].name](data[key])) {
                        req.errorText = rules[key][i].text;
                        return false;
                    }
                }
                return true;
            }
            if (!validator[rules[key].name](data[key])) {
                req.errorText = rules[key].text;
                return false;
            }
            return true;
        });
    }
};



module.exports = function validationStrategy(router, configObj, handlers) {

    var types = {
        'GET': 'query',
        "POST": 'body'
    };

    if (!configObj.config.rules) {
        throw new Error('Rules for validation middleware for route: ' + config.path + ' is not provided');
    }

    if (configObj.path) {
        router.use(configObj.path, validate);
        if (configObj.handler && handlers) {
            router.use(configObj.path, handlers[configObj.handler]);
        }
        return;
    }

    router.use(validate);
    if (configObj.handler && handlers) {
        router.use(handlers[configObj.handler]);
    }

    function validate(req, res, next) {
        var rules = configObj.config.rules,
            isValid = true,
            data = null;

        if (!helpService.validateReqMethod(configObj, req)) {
            return next();
        }

        if (!_.isEmpty(req[types[req.method]])) {
            data = req[types[req.method]];
        }

        if (!data || _.isEmpty(data)) {
            if (!configObj.handler) {
                log.warn('Validation is not passed', {
                    route: configObj.path
                });
                res.status(400).json({ error: 'Data is not provided' });
                return;
            }
            return next(new Error('Data is not provided'));
        }

        isValid = helpService.checkData(rules, data, req);

        if (!isValid && !configObj.handler) {
            log.warn('Validation is not passed', {
                route: configObj.path,
                data: data
            });
            res.status(400).json({ error: req.errorText });
            return;
        }

        isValid ? next() : next(new Error('Validation is not passed'));

    }

};