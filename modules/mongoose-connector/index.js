'use strict';

var mongoose = require('mongoose-aplus');
var config = require('config');
var log = require('logger')(module);

module.exports.init = function init() {
    var url = config.get('database:type') + '://' + config.get('database:host') + ':' + config.get('database:port');

    mongoose.connectP(url)
        .then(function () {
            log.info('Connection to mongodb is successfully established');
        })
        .catch(function (err) {
            log.error('Error occurs when connect to mongodb', err);
        });

};