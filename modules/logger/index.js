'use strict';

var winston = require('winston');

module.exports = function (module) {
    var path = module.filename.split('/').slice(-2).join('/');
    return new winston.Logger({
        transports: [
            new winston.transports.Console({
                colorize: true,
                level: 'info',
                label: path
            }),
            new winston.transports.File({
                name: 'error-file',
                filename: 'errors.log',
                level: 'error'
            })

        ]
    })
};