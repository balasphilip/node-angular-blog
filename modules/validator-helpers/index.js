var validator = require('validator');

module.exports.init = function init() {

    validator.extend('isRequired', function required(value) {
        return typeof value !== 'undefined' && value !== null && value.length !== 0 && value !== '' && value !== 0;
    });

};